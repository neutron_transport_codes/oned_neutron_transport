#include "Solver/Grid.hpp"
#include "Solver/EigenValues.hpp"
#include "Solver/HomoSolution.hpp"
#include "Solver/ExpCoefficients.hpp"
#include "Solver/ScalarFlux.hpp"
#include "Solver/ParticularSolution.hpp"

template<typename Q>
std::vector<double> solver(Q quad, double a, double b, double sigT, double c, int K, int n){
  std::clock_t start;
  start = std::clock();
  
  // ----- Set Important Parameters -----
  auto nodes = quad.getNodes();
  auto weigh = quad.getWeights();
  int N = nodes.size();  
  double sigS = c*sigT;
  std::string sType;
  if (n == 0){
    sType = "n = 0";
  } else if (n == 1){
    sType = "n = 1";
  }
  
  // ----- Build Spatial Grid -----
  std::vector<double> grid = buildGrid(a, b, K);  
    
  // ----- Build EigenValue Matrix -----
  blaze::DynamicMatrix<double, blaze::columnMajor> eigen = buildEigenMatrix(nodes, weigh, c);
  
  // ----- Solve for eigenvalues -----
  blaze::DynamicVector<blaze::complex<double>, blaze::columnVector> eigenValues(N);
  blaze::geev( eigen, eigenValues );
  
  // ----- Compute normalized eigenfunction values -----
  blaze::DynamicMatrix<double, blaze::columnMajor> homoFlux = computeHomoFlux( eigenValues, nodes );
      
  // ----- Make Particular Solutions -----
  auto constant  = getN0(sigT, sigS);
  auto quadratic = getN1(a, b, sigT, sigS); 

  // ----- Build Matrix B and Vector f -----
  blaze::DynamicMatrix<double, blaze::columnMajor>  B = buildMatrixB( homoFlux, eigenValues, a, b, sigT, N );
  blaze::DynamicVector<double, blaze::columnVector> f = buildVectorF( nodes, a, b, constant, quadratic, sType );

  // ----- Solve for expansion coefficients -----
  const std::unique_ptr<blaze::blas_int_t[]> ipiv( new blaze::blas_int_t[N] );
  blaze::gesv( B, f, ipiv.get() );

  // ----- Generate Scalar Flux Function -----
  std::vector<double> scalar = computeScalarFlux( homoFlux, f, eigenValues, nodes, weigh,
						  grid, sigT, constant, quadratic, sType);

  // ----- Output Finish Message ----- 
  double duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
  std::cout << "##############################################" << std::endl;
  std::cout << "######## ANALYTIC Sn SOLVER COMPLETE #########" << std::endl;
  std::cout << "##############################################" << std::endl;
  std::cout << "- run time: " << duration << " sec" << std::endl;
  std::cout << "##############################################" << std::endl << std::endl;
  
  return scalar;
}
