template<typename N, typename W, typename C, typename Q>
auto computeScalarFlux(blaze::DynamicMatrix<double, blaze::columnMajor> homoFlux,
		       blaze::DynamicVector<double, blaze::columnVector> coeff,
		       blaze::DynamicVector<blaze::complex<double>, blaze::columnVector> eigenValues,
		       N nodes, W weigh, std::vector<double> grid, double sigT, C constant_s, Q quadratic_s,
		       std::string sType){

  int K = grid.size() - 1;
  int M = nodes.size();
  double a = grid[0];
  double b = grid[K];

  // Compute Cell Averaged Scalar
  std::vector<double> flux(K, 0.0);

  for(int i = 0; i < K; ++i){
    double xR = grid[i+1];
    double xL = grid[i+0];
    double dx = xR - xL;

    double homo = 0.0;
    double part = 0.0;
    for(int m = 0; m < M; ++m){
      double pos = 0;
      double neg = 0;

      for(int k = 0; k < M; ++k){
	double nu_k = 1/eigenValues[k].real();
	if( nu_k > 0 ){
	  pos += coeff[k]*homoFlux(m,k)*( exp( sigT*(xR - b)/nu_k ) - exp( sigT*(xL - b)/nu_k ) )*( nu_k/(sigT*dx) );
	} else {
	  neg += coeff[k]*homoFlux(m,k)*( exp( sigT*(xR - a)/nu_k ) - exp( sigT*(xL - a)/nu_k ) )*( nu_k/(sigT*dx) );
	}
      }
      homo += weigh[m]*( pos + neg );
      
      if( sType == "n = 0"){
	auto kernel = boost::hana::curry<2>(constant_s) (nodes[m])();
	double error = 0.0;
	double uniform = boost::math::quadrature::gauss_kronrod<double, 15>::integrate(kernel, xL, xR, 0, 0, &error);
	part += weigh[m]*( uniform/dx );
      } else {
	auto kernel = boost::hana::curry<2>(quadratic_s) (nodes[m])();
	double error = 0.0;
	double quadratic = boost::math::quadrature::gauss_kronrod<double, 15>::integrate(kernel, xL, xR, 0,0,&error);
	part += weigh[m]*( quadratic/dx );
      }
    }
    flux[i] = homo + part;
  }

  return flux;
}
