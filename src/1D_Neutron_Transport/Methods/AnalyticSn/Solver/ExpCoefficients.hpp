blaze::DynamicMatrix<double, blaze::columnMajor> buildMatrixB(blaze::DynamicMatrix<double, blaze::columnMajor> homoFlux, blaze::DynamicVector<blaze::complex<double>, blaze::columnVector> eigenValues, double a, double b, double sigT, int N){
  blaze::DynamicMatrix<double, blaze::columnMajor> B(N, N);
  for( int m = 0; m < N; ++m ){

    for( int k = 0; k < N; ++k ){
      double nu_k = 1/eigenValues[k].real();

      if( m < static_cast<int>(N/2) ){
	if( nu_k > 0 ) {
	  B(m,k) = homoFlux(m,k)*std::exp( sigT*(a - b)/nu_k );
	} else {
	  B(m,k) = homoFlux(m,k)*std::exp( sigT*(a - a)/nu_k );
	}
      } else {
	if( nu_k > 0 ){
	  B(m,k) = homoFlux(m,k)*std::exp( sigT*(b - b)/nu_k );
	} else {
	  B(m,k) = homoFlux(m,k)*std::exp( sigT*(b - a)/nu_k );
	}
      }
      
    }
    
  }
  return B;
}


template<typename T, typename U, typename Q>
blaze::DynamicVector<double, blaze::columnVector> buildVectorF(T&& nodes, double a, double b, U&& uniform,
							       Q&& quadratic,  std::string sType){
  int N = nodes.size();
  blaze::DynamicVector<double, blaze::columnVector> f(N, 0.0);
  for( int i = 0; i < N; ++i ){
    if( i < static_cast<int>(N/2) ){
      if( sType == "n = 0" ){
	f[i] = (-1)*uniform(nodes[i], a);
      } else {
	f[i] = (-1)*quadratic(nodes[i], a);
      }
    } else {
      if( sType == "n = 0" ){
	f[i] = (-1)*uniform(nodes[i], b);
      } else {
	f[i] = (-1)*quadratic(nodes[i], b);
      }
    }
  }   
  return f;
}
