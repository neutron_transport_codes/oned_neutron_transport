template<typename N, typename W>
blaze::DynamicMatrix<double, blaze::columnMajor> buildEigenMatrix(N&& nodes, W&& weights, double c){
  int I = nodes.size();
  blaze::DynamicMatrix<double, blaze::columnMajor> A(I, I);
  for( int i = 0; i < I; ++i ){
    for( int j = 0; j < I; ++j ){
      if( i == j ){
	A(i,j) = c/(2*nodes[i])*weights[j] - 1/nodes[i];
      } else {
	A(i,j) = c/(2*nodes[i])*weights[j];
      }
    }
  }
  return A;
}
