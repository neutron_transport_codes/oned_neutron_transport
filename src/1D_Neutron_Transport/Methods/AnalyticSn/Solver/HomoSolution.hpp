template<typename T>
blaze::DynamicMatrix<double, blaze::columnMajor> computeHomoFlux(blaze::DynamicVector<blaze::complex<double>, blaze::columnVector> eigenValues, T nodes){
  int N = nodes.size();

  blaze::DynamicMatrix<double, blaze::columnMajor> flux(N, N);

  for( int i = 0; i < N; ++i ){
    for( int j = 0; j < N; ++j ){
      double nu_k = 1/eigenValues[j].real();
      flux(i,j) = 1/(1 + nodes[i]/nu_k);
    }
  }

  return flux;
}
