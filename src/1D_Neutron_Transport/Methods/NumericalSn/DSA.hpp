class DSA {
  Q quad;
  double sigT, c, sigA, sigS;
  int K;

  blaze::DynamicMatrix<double, blaze::columnMajor> B;
  blaze::StaticMatrix<double, 2, 2, blaze::columnMajor> M{ {1.0/3.0, 1.0/6.0},
							   {1.0/6.0, 1.0/3.0} };
  
public:
  std::vector<double> grid_d;

  DSA(Q& quad, double sigT, double c, int K) : quad(quad), sigT(sigT), c(c), K(K), B(4*K, 4*K, 0.0), grid_d(K+1,0.0)
  {
    this->sigA = sigT*(1.0 - c);
    this->sigS = sigT*c;
  }

  double alpha, beta;
  
  /* Member Functions */
#include "DSA/Build.hpp"

  blaze::DynamicVector<double, blaze::columnVector> solve(blaze::DynamicVector<double, blaze::columnVector> scalar_L,
							  blaze::DynamicVector<double, blaze::columnVector> scalar_R,
							  blaze::DynamicVector<double, blaze::columnVector> scalar_Lp,
							  blaze::DynamicVector<double, blaze::columnVector> scalar_Rp)
  {
    /* Build Source */
    blaze::DynamicVector<double, blaze::columnVector> z(4*this->K, 0.0);
    for( int i = 0; i < this->K; ++i ){
      double s_L = scalar_Lp[i] - scalar_L[i];
      double s_R = scalar_Rp[i] - scalar_R[i];
      double dx = this->grid_d[i+1] - this->grid_d[i];
      z[4*i+0] = this->sigS*dx*( (1.0/3.0)*s_L + (1.0/6.0)*s_R );
      z[4*i+2] = this->sigS*dx*( (1.0/6.0)*s_L + (1.0/3.0)*s_R );
    }
    
    /* Solve */
    int M = 4*this->K;
    blaze::DynamicMatrix<double, blaze::columnMajor> A = this->B;

    const std::unique_ptr<blaze::blas_int_t[]> ipiv( new blaze::blas_int_t[M] );
    blaze::gesv( A, z, ipiv.get() );

    return z;
  }
};
