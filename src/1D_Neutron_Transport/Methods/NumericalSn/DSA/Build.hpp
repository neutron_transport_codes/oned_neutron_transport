void calcAlphaBeta(){
  auto nodes = this->quad.getNodes();
  auto weigh = this->quad.getWeights();
  int N = nodes.size();
  for(int n = 0; n < N; ++n){
    if(nodes[n] > 0) {
      this->alpha += 0.5*weigh[n]*nodes[n];
      this->beta  += 1.5*weigh[n]*nodes[n]*nodes[n];
    }
  }
  this->alpha = 0.25;
  this->beta  = 0.50;
}

void buildB(){
  for(int i = 0; i < this->K; ++i){
    double dx = this->grid_d[i+1] - this->grid_d[i+0];
    for(int j = 0; j < this->K; ++j){
      if(j == i){
      	this->B(4*i+0, 4*i+0) = this->alpha/(2.0*this->beta) + this->sigA*dx/3.0;
      	this->B(4*i+0, 4*i+1) = 0.0;
      	this->B(4*i+0, 4*i+2) = this->sigA*dx/6.0;
      	this->B(4*i+0, 4*i+3) = 0.5;

      	this->B(4*i+1, 4*i+0) = 0.0;
      	this->B(4*i+1, 4*i+1) = this->beta/(2.0*this->alpha) + this->sigT*dx;
      	this->B(4*i+1, 4*i+2) = 0.5;
      	this->B(4*i+1, 4*i+3) = this->sigT*dx/2.0;

      	this->B(4*i+2, 4*i+0) = this->sigA*dx/6.0;
      	this->B(4*i+2, 4*i+1) = -0.5;
      	this->B(4*i+2, 4*i+2) = this->alpha/(2.0*this->beta) + this->sigA*dx/3.0;
      	this->B(4*i+2, 4*i+3) = 0.0;

      	this->B(4*i+3, 4*i+0) = -0.5;
      	this->B(4*i+3, 4*i+1) = this->sigT*dx/2.0;
      	this->B(4*i+3, 4*i+2) = 0.0;
      	this->B(4*i+3, 4*i+3) = this->beta/(2.0*this->alpha) + this->sigT*dx;

      } else if (j == i-1){
      	this->B(4*i+0, 4*j+2) = (-1.0)*this->alpha/(2.0*this->beta);
      	this->B(4*i+0, 4*j+3) = -0.5;

      	this->B(4*i+1, 4*j+2) = -0.5;
      	this->B(4*i+1, 4*j+3) = (-1.0)*this->beta/(2.0*this->alpha);

      } else if (j == i+1){
      	this->B(4*i+2, 4*j+0) = (-1.0)*this->alpha/(2.0*this->beta);
      	this->B(4*i+2, 4*j+1) = 0.5;

      	this->B(4*i+3, 4*j+0) = 0.5;
      	this->B(4*i+3, 4*j+1) = (-1.0)*this->beta/(2.0*this->alpha);
      }
    }
  }
}
