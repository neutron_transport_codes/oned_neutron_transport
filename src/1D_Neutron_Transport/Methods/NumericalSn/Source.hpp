blaze::DynamicVector<double, blaze::columnVector> getSource(double mu, int i) {
  auto BL = [](double xL, double xR, double x){ return (xR - x)/(xR - xL); };
  auto BR = [](double xL, double xR, double x){ return (x - xL)/(xR - xL); };
  double xL = this->grid[i+0];
  double xR = this->grid[i+1];
  auto BLi = boost::hana::curry<3>(BL) (xL)(xR)();
  auto BRi = boost::hana::curry<3>(BR) (xL)(xR)();
  auto f1 = [BLi, this, mu](auto x){ return BLi(x)*this->source(x, mu); };
  auto f2 = [BRi, this, mu](auto x){ return BRi(x)*this->source(x, mu); };
  
  double error = 0.0;
  double qL = boost::math::quadrature::gauss_kronrod<double, 7>::integrate(f1, xL, xR, 0, 0, &error);
  double qR = boost::math::quadrature::gauss_kronrod<double, 7>::integrate(f2, xL, xR, 0, 0, &error);

  blaze::DynamicVector<double, blaze::columnVector> vec{ qL, qR };
  return vec;
}
