double computeBalance() {
  auto angles  = this->quad.getNodes();
  auto weights = this->quad.getWeights();
  
  // Compute Partial Currents
  double current_L = 0.0;
  double current_R = 0.0;
  for( int m = 0; m < this->N; ++m ){
    current_L += weights[m]*angles[m]*this->angular_L[m];
    current_R += weights[m]*angles[m]*this->angular_R[m];
  }

  // Compute Absorption
  double absorption  = 0.0;
  double sigA = this->sigT - this->sigS;
  for( int i = 0; i < this->K; ++i ){
    absorption += 0.5*sigA*(this->grid[i+1] - this->grid[i])*(this->scalar_L[i] + this->scalar_R[i]);
  }

  // Compute Source
  double source_sum = 0.0;
  for( int m = 0; m < this->N; ++m ){
    for( int i = 0; i < this->K; ++i ){
      auto source = this->getSource(angles[m], i);
      source_sum += weights[m]*(source[0] + source[1]);
    }
  }

  double bal = source_sum - absorption - (current_R - current_L);
  return bal;
}
