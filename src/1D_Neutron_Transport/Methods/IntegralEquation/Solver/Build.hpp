double E2(double x){
  return std::exp( (-1)*x ) + x*std::expint( (-1)*x );
}

double getR(double x, double b){
  return 2.0 - E2(x) - E2(b - x);
}

template<typename T>
blaze::DynamicMatrix<double, blaze::columnMajor> buildA(T&& grid, T&& weights, double b, double c){
  int N = grid.size();
  blaze::DynamicMatrix<double, blaze::columnMajor> A(N, N, 0.0);
  for( int i = 0; i < N; ++i ){
    for( int j = 0; j < N; ++j ){

      if( i == j){
	double sum = 0.0;
	for( int k = 0; k < N; ++k ){
	  if( k != i ){
	    sum += (-1.0)*std::expint( (-1.0)*abs(grid[i] - grid[k]) )*weights[k];
	  }
	}
	A(i, j) = 1.0 - (c/2.0)*getR(grid[i], b) + (c/2.0)*sum;
      } else {
	A(i, j) = (c/2.0)*std::expint( (-1.0)*abs( grid[i] - grid[j] ) )*weights[j];
      }
      
    }
  }
  return A;
}

template<typename T, typename S>
blaze::DynamicVector<double, blaze::columnVector> buildB(T&& grid, T&& weights, double b, double sigT, S source) {
  int N = grid.size();

  blaze::DynamicVector<double, blaze::columnVector> B(N, 0.0);
  
  for( int i = 0; i < N; ++i ){
    double sum = 0.0;
    for( int k = 0; k < N; ++k ){
      if( k != i ){
	sum += (-1)*std::expint( (-1)*abs(grid[i] - grid[k]) )*weights[k]*( source(grid[k], 1.0)
									    - source(grid[i], 1.0) );
      }
    }
    B[i] = (1.0/sigT)*source(grid[i], 1.0)*getR(grid[i], b) + (1.0/sigT)*sum;
  }
  
  return B;
}
