template<typename N>
std::vector<double> makeGrid(N&& nodes, double a, double b){
  std::vector<double> grid(nodes.size(), 0.0);

  for( int i = 0; i <= static_cast<int>(nodes.size()); ++i ){
    grid[i] = (b - a)/2.0*(-1)*nodes[i] + (b + a)/2.0;
  }
  
  return grid;
}

template<typename W>
std::vector<double> makeWeights(W&& weights, double a, double b){
  std::vector<double> grid(weights.size(), 0.0);

  for( int i = 0; i <= static_cast<int>(weights.size()); ++i ){
    grid[i] = (b - a)/2.0*weights[i];
  }
  
  return grid;
}
