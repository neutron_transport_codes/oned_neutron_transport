#include "Solver/Grid.hpp"
#include "Solver/Build.hpp"

template<typename Q, typename S>
auto solver (Q quad, double a, double b, double sigT, double c, S source) {
  std::clock_t start;
  start = std::clock();

  // make xgrid
  auto nodes = quad.getNodes();
  auto weigh = quad.getWeights();
  int K = nodes.size();

  auto grid    = makeGrid(nodes, a, b); 
  auto weights = makeWeights(weigh, a, b);

  // ----- Build System of Equations -----
  blaze::DynamicMatrix<double, blaze::columnMajor>  A = buildA(grid, weights, b, c);
  blaze::DynamicVector<double, blaze::columnVector> C = buildB(grid, weights, b, sigT, source);

  // ----- Solve System of Equations -----
  int N = nodes.size();
  const std::unique_ptr<blaze::blas_int_t[]> ipiv( new blaze::blas_int_t[N] ); 
  blaze::gesv( A, C, ipiv.get() );

  // ----- Compute Scalar Flux -----
  std::vector< std::vector<double> > scalar(2, std::vector<double>(K, 0.0) );
  for(int i = 0; i < K; ++i){
    scalar[0][i] = grid[i];
    scalar[1][i] = C[i];
  }
  
  double duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
  std::cout << "##############################################" << std::endl;
  std::cout << "########## INTEGRAL SOLVER COMPLETE ##########" << std::endl;
  std::cout << "##############################################" << std::endl;
  std::cout << "- run time: " << duration << " sec" << std::endl;
  std::cout << "##############################################" << std::endl << std::endl;

  
  //  std::cout << "- Integral Equation Solver Completed in: " << duration << std::endl;
  return scalar;
}
