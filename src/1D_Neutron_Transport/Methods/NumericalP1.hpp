template<typename Q, typename S, typename J>
class NumericalP1 {
  Q quad;
  double a, b, sigT, c, sigA;
  int K;
  S source_0;
  J source_1;

  std::vector<double> grid, flux, current;

  double alpha = 0.25;
  double beta  = 0.50;
  
  blaze::DynamicMatrix<double, blaze::columnMajor> A;
  blaze::DynamicVector<double, blaze::columnVector> B;

  /* hidden functions */
#include "NumericalP1/Grid.hpp"
#include "NumericalP1/Build.hpp"

  void calcAlphaBeta(){
    auto nodes = this->quad.getNodes();
    auto weigh = this->quad.getWeights();
    int N = nodes.size();
    for(int n = 0; n < N; ++n){
      if(nodes[n] > 0) {
	this->alpha += 0.5*weigh[n]*nodes[n];
	this->beta  += 1.5*weigh[n]*nodes[n]*nodes[n];
      }
    }
  }

  void cellAverage(){
    for(int i = 0; i < this->K; ++i) {
      this->flux[i]    = 0.5*(this->B[4*i+0] + this->B[4*i+2]);
      this->current[i] = 0.5*(this->B[4*i+1] + this->B[4*i+3]);
    }
  }
  
public:
  NumericalP1( Q quad, double a, double b, double sigT, double c, int K, S source_0, J source_1)
    : quad(quad), a(a), b(b), sigT(sigT), c(c), K(K), source_0(source_0), source_1(source_1),
      grid(K+1, 0.0), flux(K, 0.0), current(K, 0.0), A(4*K, 4*K, 0.0), B(4*K, 0.0)
  {
    this->sigA = sigT*(1.0 - c);
  }

  void solve(){
    std::clock_t start;
    start = std::clock();    

    /* Generate Problem */
    // this->calcAlphaBeta();
    this->buildGrid();
    this->buildB();
    this->buildA();

    /* Solve System of Equations */
    int M = 4*this->K;
    const std::unique_ptr<blaze::blas_int_t[]> ipiv( new blaze::blas_int_t[M] );
    blaze::gesv( this->A, this->B, ipiv.get() );

    /* Compute Cell Averaged Flux */
    this->cellAverage();
    
    double duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
  
    //  Output Results to Screen
    std::cout << "##############################################" << std::endl;
    std::cout << "######## NUMERICAL P1 SOLVER COMPLETE ########" << std::endl;
    std::cout << "##############################################" << std::endl;
    std::cout << "- total time:  " << duration << " sec" << std::endl;
    std::cout << "##############################################" << std::endl << std::endl;
  }

  std::vector<double> getGrid(){
    return this->grid;
  }

  std::vector<double> getFlux(){
    return this->flux;
  }

  std::vector<double> getCurrent(){
    return this->current;
  }
};
