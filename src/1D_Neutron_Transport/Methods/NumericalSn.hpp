template<typename Q, typename L, typename R, typename S>
class NumericalSn {

  Q quad;
  double a, b, sigT, c, sigS;
  int N, K;
  L bound_L;
  R bound_R;
  S source;

  std::vector<double> grid, scalar;

  blaze::DynamicVector<double, blaze::columnVector> scalar_L, scalar_R, angular_L, angular_R;

  blaze::StaticMatrix<double, 2, 2, blaze::columnMajor> L_p{ { 0.5, 0.5},
							     {-0.5, 0.5} };

  blaze::StaticMatrix<double, 2, 2, blaze::columnMajor> V_p{ {0.0, 1.0},
							     {0.0, 0.0} };
  
  blaze::StaticMatrix<double, 2, 2, blaze::columnMajor> L_m{ {-0.5,  0.5},
							     {-0.5, -0.5} };

  blaze::StaticMatrix<double, 2, 2, blaze::columnMajor> V_m{ { 0.0, 0.0},
							     {-1.0, 0.0} };

  blaze::StaticMatrix<double, 2, 2, blaze::columnMajor> M{ {1.0/3.0, 1.0/6.0},
							   {1.0/6.0, 1.0/3.0} };
  
  /* Private Functions */
#include "NumericalSn/Grid.hpp"
#include "NumericalSn/Source.hpp"
#include "NumericalSn/Balance.hpp"

  /* Private Classes */
#include "NumericalSn/DSA.hpp"
  DSA dsa;
  
public:
  NumericalSn(Q& quad, double a, double b, double sigT, double c, int K, L bound_L, R bound_R, S source)
    : quad(quad), a(a), b(b), sigT(sigT), c(c), K(K), bound_L(bound_L), bound_R(bound_R), source(source),
      grid(K+1, 0.0), scalar(K, 0.0), scalar_L(K, 0.0), scalar_R(K, 0.0), angular_L(quad.order(), 0.0),
      angular_R(quad.order(), 0.0), dsa(quad, sigT, c, K)
  {
    this->sigS = c*sigT;
    this->N    = this->quad.order();
    
    this->buildGrid();
  }

  void solve(double tolerance, bool dsa_t) {
    std::clock_t start;
    start = std::clock();
    
    auto angles  = this->quad.getNodes();
    auto weights = this->quad.getWeights();

    /* Initialize DSA */
    if (dsa_t) {
      this->dsa.grid_d = this->grid;
      this->dsa.calcAlphaBeta();
      this->dsa.buildB();
    }
    
    /* Allocations in Memory */
    blaze::DynamicVector<double, blaze::columnVector> scalar_Lp(this->K, 0.0);
    blaze::DynamicVector<double, blaze::columnVector> scalar_Rp(this->K, 0.0);
    blaze::DynamicVector<double, blaze::columnVector> upwind(2, 0.0);
    blaze::DynamicVector<double, blaze::columnVector> Phi_i (2, 0.0);
    blaze::DynamicVector<double, blaze::columnVector> source(2, 0.0);

    blaze::DynamicMatrix<double, blaze::columnMajor>  A(2, 2, 0.0);
    blaze::DynamicVector<double, blaze::columnVector> b(2, 0.0);
    
    double mu_n = 0.0;
    double we_n = 0.0;
    double dx   = 0.0;

    int M = 2;
    const std::unique_ptr<blaze::blas_int_t[]> ipiv( new blaze::blas_int_t[M] );
    
    /* Source Iteration */
    bool condition = true;
    int it = 0;
    double error = 1.0;
    double error_p = 1.0;
    double rho = 0.0;
    double mod_tolerance = tolerance;

    do {
      /* Sweep in Angle */
      for(int n = 0; n < this->N; ++n) {
	mu_n = angles[n];
	we_n = weights[n];
	
	if(mu_n > 0) {
	  upwind[0] = this->bound_L(mu_n);
	  upwind[1] = this->bound_L(mu_n);
	  this->angular_L[n] = this->bound_L(mu_n);
	  
	  for(int i = 0; i < this->K; ++i) {
	    dx = this->grid[i+1] - this->grid[i];
	    Phi_i[0] = this->scalar_L[i];
	    Phi_i[1] = this->scalar_R[i];
	    source = this->getSource(mu_n, i);

	    /* Build Matrix A */
	    A = mu_n*this->L_p + this->sigT*dx*this->M;

	    /* Build Vector b */
	    b = 0.5*this->sigS*dx*this->M*Phi_i + source + mu_n*this->V_p*upwind;

	    /* Solve 2x2 System */
	    blaze::gesv( A, b, ipiv.get() );

	    /* Update Upwinding */
	    upwind[0] = b[0];
	    upwind[1] = b[1];

	    /* Update Scalar Flux */
	    scalar_Lp[i] += we_n*b[0];
	    scalar_Rp[i] += we_n*b[1];

	    /* If at right bound update angular flux */
	    if(i == this->K-1) {
	      this->angular_R[n] = b[1];
	    }
	  }
	} else {
	  upwind[0] = this->bound_R(mu_n);
	  upwind[1] = this->bound_R(mu_n);
	  this->angular_R[n] = this->bound_R(mu_n);

	  for( int i = K-1; i >= 0; --i ){
	    dx = this->grid[i+1] - this->grid[i];
	    Phi_i[0] = this->scalar_L[i];
	    Phi_i[1] = this->scalar_R[i];
	    source = this->getSource(mu_n, i);

	    /* Build Matrix A */
	    A = mu_n*this->L_m + this->sigT*dx*this->M;

	    /* Build Vector b */
	    b = 0.5*this->sigS*dx*this->M*Phi_i + source + mu_n*this->V_m*upwind;

	    /* Solve 2x2 System */
	    blaze::gesv( A, b, ipiv.get() );

	    /* Update Upwinding */
	    upwind[0] = b[0];
	    upwind[1] = b[1];

	    /* Update Scalar Flux */
	    scalar_Lp[i] += we_n*b[0];
	    scalar_Rp[i] += we_n*b[1];

	    /* If at left bound update angular flux */
	    if(i == 0) {
	      this->angular_L[n] = b[0];
	    }
	  }
	}
      }

      // DSA
      if (dsa_t) {
      	blaze::DynamicVector<double, blaze::columnVector> d = this->dsa.solve(scalar_L, scalar_R,
									      scalar_Lp, scalar_Rp);
      	for( int i = 0; i < this->K; ++i ){
      	  scalar_Lp[i] += d[4*i+0];
	  scalar_Rp[i] += d[4*i+2];
	}
      }
      
      /* Compute Error */
      error_p = blaze::l2Norm((scalar_Lp - this->scalar_L) + (scalar_Rp - this->scalar_R) )/pow(this->K, 0.5);
      rho = error_p/error;

      mod_tolerance = tolerance*( (1 - rho)/(rho) );

      /* Check for Convergence */
      if(error_p < mod_tolerance || it > 1e5) {
	condition = false;
      } else {
	it += 1;
      }

      /* Update Scalar Fluxes and Error */
      this->scalar_L = scalar_Lp;
      this->scalar_R = scalar_Rp;
      error = error_p;

      /* Zero Out Scalar Fluxes */
      scalar_Lp.reset();
      scalar_Rp.reset();
      
    } while(condition);

    /* Calculate Cell Average Scalar Flux */
    for(int i = 0; i < this->K; ++i){
      this->scalar[i] = 0.5*(this->scalar_L[i] + this->scalar_R[i]);
    }

    
    /* Compute Balance */
    double balance = this->computeBalance();
    
    /* Output Results */
    double duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout << "##############################################" << std::endl;
    std::cout << "######## NUMERICAL Sn SOLVER COMPLETE ########" << std::endl;
    std::cout << "##############################################" << std::endl;
    std::cout << "- total time:   " << duration << " sec" << std::endl;
    std::cout << "- iteration #:  " << it << std::endl;
    std::cout << "- spectral rad: " << rho << std::endl;
    std::cout << "- final error:  " << error << std::endl;
    std::cout << "- balance:      " << balance << std::endl;
    std::cout << "##############################################" << std::endl << std::endl;
  }
  
  std::vector<double> getScalar() {
    return this->scalar;
  }

};
