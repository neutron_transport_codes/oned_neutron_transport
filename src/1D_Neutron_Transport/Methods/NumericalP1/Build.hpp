void buildB(){
  auto BL = [](double xL, double xR, double x){ return (xR - x)/(xR - xL); };
  auto BR = [](double xL, double xR, double x){ return (x - xL)/(xR - xL); };
  for(int i = 0; i < this->K; ++i){
    double xL = this->grid[i+0];
    double xR = this->grid[i+1];
    
    auto BLi = boost::hana::curry<3>(BL) (xL)(xR)();
    auto BRi = boost::hana::curry<3>(BR) (xL)(xR)();
    auto f1 = [BLi, this](auto x){ return BLi(x)*this->source_0(x); };
    auto f2 = [BRi, this](auto x){ return BRi(x)*this->source_0(x); };

    auto f3 = [BLi, this](auto x){ return BLi(x)*this->source_1(x); };
    auto f4 = [BRi, this](auto x){ return BRi(x)*this->source_1(x); };
    
    double error = 0.0;
    double qL_0 = boost::math::quadrature::gauss_kronrod<double, 15>::integrate(f1, xL, xR, 0, 0, &error);
    double qR_0 = boost::math::quadrature::gauss_kronrod<double, 15>::integrate(f2, xL, xR, 0, 0, &error);
    double qL_1 = boost::math::quadrature::gauss_kronrod<double, 15>::integrate(f3, xL, xR, 0, 0, &error);
    double qR_1 = boost::math::quadrature::gauss_kronrod<double, 15>::integrate(f4, xL, xR, 0, 0, &error);

    this->B[4*i+0] = qL_0;
    this->B[4*i+1] = qL_1;
    this->B[4*i+2] = qR_0;
    this->B[4*i+3] = qR_1;
  }
}

void buildA(){
  for(int i = 0; i < this->K; ++i){
    double dx = this->grid[i+1] - this->grid[i+0];
    for(int j = 0; j < this->K; ++j){
      if(j == i){
	this->A(4*i+0, 4*i+0) = this->alpha/(2.0*this->beta) + this->sigA*dx/3.0;
	this->A(4*i+0, 4*i+1) = 0.0;
	this->A(4*i+0, 4*i+2) = this->sigA*dx/6.0;
	this->A(4*i+0, 4*i+3) = 0.5;

	this->A(4*i+1, 4*i+0) = 0.0;
	this->A(4*i+1, 4*i+1) = this->beta/(2.0*this->alpha) + this->sigT*dx;
	this->A(4*i+1, 4*i+2) = 0.5;
	this->A(4*i+1, 4*i+3) = this->sigT*dx/2.0;

	this->A(4*i+2, 4*i+0) = this->sigA*dx/6.0;
	this->A(4*i+2, 4*i+1) = -0.5;
	this->A(4*i+2, 4*i+2) = this->alpha/(2.0*this->beta) + this->sigA*dx/3.0;
	this->A(4*i+2, 4*i+3) = 0.0;

	this->A(4*i+3, 4*i+0) = -0.5;
	this->A(4*i+3, 4*i+1) = this->sigT*dx/2.0;
	this->A(4*i+3, 4*i+2) = 0.0;
	this->A(4*i+3, 4*i+3) = this->beta/(2.0*this->alpha) + this->sigT*dx;

      } else if (j == i-1){
	this->A(4*i+0, 4*j+2) = (-1.0)*this->alpha/(2.0*this->beta);
	this->A(4*i+0, 4*j+3) = -0.5;

	this->A(4*i+1, 4*j+2) = -0.5;
	this->A(4*i+1, 4*j+3) = (-1.0)*this->beta/(2.0*this->alpha);

      } else if (j == i+1){
	this->A(4*i+2, 4*j+0) = (-1.0)*this->alpha/(2.0*this->beta);
	this->A(4*i+2, 4*j+1) = 0.5;

	this->A(4*i+3, 4*j+0) = 0.5;
	this->A(4*i+3, 4*j+1) = (-1.0)*this->beta/(2.0*this->alpha);
      }
    }
  }
}
