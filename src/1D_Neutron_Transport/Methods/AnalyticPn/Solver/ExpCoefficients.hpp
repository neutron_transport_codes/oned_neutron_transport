template<typename T, typename U, typename Q>
blaze::DynamicVector<double, blaze::columnVector> buildF(T&& nodes, double a, double b, U&& uniform,
							 Q&& quadratic, std::string sType){
  int N = nodes.size();
  blaze::DynamicVector<double, blaze::columnVector> f(N, 0.0);
  for( int i = 0; i < N; ++i ){
    if( i < static_cast<int>(N/2) ){
      if( sType == "n = 0" ){
	f[i] = (-1)*uniform(nodes[i], a);
      } else {
	f[i] = (-1)*quadratic(nodes[i], a);
      }
    } else {
      if( sType == "n = 0" ){
	f[i] = (-1)*uniform(nodes[i], b);
      } else {
	f[i] = (-1)*quadratic(nodes[i], b);
      }
    }
  }
  return f;
}

template<typename T>
blaze::DynamicMatrix<double, blaze::columnMajor> buildB(blaze::DynamicVector<blaze::complex<double>, blaze::columnVector> eigenValues, blaze::DynamicMatrix<blaze::complex<double>, blaze::columnVector> eigenVec, T&& nodes, double a, double b, double sigT){
  int N = nodes.size();
  blaze::DynamicMatrix<double, blaze::columnMajor> B(N, N, 0.0);

  for(int m = 0; m < N; ++m){
    for(int k = 0; k < N; ++k){
      double nu_k = eigenValues[k].real();
      double sum  = 0.0;
      for( int n = 0; n < N; ++n ){
	sum += 0.5*( 2*static_cast<double>(n) + 1 )*eigenVec(n,k).real()*std::legendre(n,nodes[m]);
      }
      if( m < static_cast<int>(N/2) ){
	B(m,k) = exp(sigT*a*nu_k)*sum;
      } else {
	B(m,k) = exp(sigT*b*nu_k)*sum;
      }
		     
    }
  }
  return B;
}
