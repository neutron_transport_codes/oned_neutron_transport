auto getN0(double sigT, double sigS){
  return [sigT, sigS](double mu, double x){ return 0*x + 0*mu + 1.0/(2*(sigT - sigS)); };
}

auto getN1(double a, double b, double sigT, double sigS){
  double c0 = (-4)*a*b/( (b - a)*(b - a) );
  double c1 =  (4)*(a + b)/( (b - a)*(b - a) );
  double c2 = (-4)/( (b - a)*(b - a) );

  auto b0 = [sigT, sigS](double x, double mu){ return 0*x + 0*mu + 1/( 2*(sigT - sigS) ); };
  auto b1 = [sigT, sigS](double x, double mu){ return 1/( 2*(sigT - sigS) )*x - 1/( 2*(sigT - sigS)*sigT )*mu; };
  auto b2 = [sigT, sigS](double x, double mu){ return 1/( 2*(sigT - sigS) )*x*x - 1/( (sigT - sigS)*sigT )*mu*x
      + 1/( (sigT - sigS)*sigT*sigT )*mu*mu + sigS/( 3*(sigT - sigS)*(sigT - sigS)*sigT*sigT ); };

  return [c0, c1, c2, b0, b1, b2](double mu, double x){ return c0*b0(x, mu) + c1*b1(x, mu) + c2*b2(x, mu); };
}
