blaze::DynamicMatrix<double, blaze::columnMajor> buildV(double sigT, double sigS, int N){
  blaze::DynamicMatrix<double, blaze::columnMajor> V(N, N, 0.0);
  for( int i = 0; i < N; ++i ){
    if( i == 0 ){
      V(i,i) = sigT - sigS;
    } else {
      V(i,i) = (2*static_cast<double>(i) + 1)*sigT;
    }
  }
  return V;
}

blaze::DynamicMatrix<double, blaze::columnMajor> buildW(double sigT, int N){
  blaze::DynamicMatrix<double, blaze::columnMajor> W(N, N, 0.0);
  for( int i = 0; i < N; ++i ){
    if( i == 0 ){
      W(i,i+1) = sigT;
    } else if( i == N-1 ){
      W(i,i-1) = static_cast<double>(i)*sigT;
    } else {
      W(i,i+1) = static_cast<double>(i+1)*sigT;
      W(i,i-1) = static_cast<double>(i+0)*sigT;
    }
  }
  return W;
}
  
