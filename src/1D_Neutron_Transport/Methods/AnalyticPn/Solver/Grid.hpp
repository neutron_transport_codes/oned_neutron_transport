std::vector<double> buildGrid(double a, double b, int K){
  std::vector<double> grid(K+1, 0.0);
  for(int i = 0; i < K+1; ++i){
    grid[i] = a + (static_cast<double>(i))/(K)*(b - a);
  }
  return grid;
}
