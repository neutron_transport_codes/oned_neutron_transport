template<typename N, typename W, typename C, typename Q>
auto computeScalarFlux(blaze::DynamicMatrix<blaze::complex<double>, blaze::columnMajor> eigenVec,
		       blaze::DynamicVector<blaze::complex<double>, blaze::columnVector> eigenValues,
		       blaze::DynamicVector<double, blaze::columnVector> coeff,
		       N nodes, W weigh, std::vector<double> grid, double sigT, C constant_s, Q quadratic_s,
		       std::string sType){

  int K = grid.size() - 1;
  int M = nodes.size();
  
  // Compute Cell Averaged Scalar
  std::vector<double> flux(K, 0.0);

  for(int i = 0; i < K; ++i){
    double xR = grid[i+1];
    double xL = grid[i+0];
    double dx = xR - xL;

    double homo = 0.0;
    for(int k = 0; k < M; ++k){
      homo += coeff[k]*eigenVec(0,k).real()*(exp(sigT*xR*eigenValues[k].real()) - exp(sigT*xL*eigenValues[k].real()))
	*(1.0/(sigT*dx*eigenValues[k].real()));
    }

    double part = 0.0;
    for(int m = 0; m < M; ++m){
      if( sType == "n = 0"){
	auto kernel = boost::hana::curry<2>(constant_s) (nodes[m])();
	double error = 0.0;
	double uniform = boost::math::quadrature::gauss_kronrod<double, 15>::integrate(kernel, xL, xR, 0,0,&error);
	part += weigh[m]*( uniform/dx );
      } else {
	auto kernel = boost::hana::curry<2>(quadratic_s) (nodes[m])();
	double error = 0.0;
	double quadratic = boost::math::quadrature::gauss_kronrod<double, 15>::integrate(kernel, xL, xR, 0,0,&error);
	part += weigh[m]*( quadratic/dx );
      }
    }
    flux[i] = homo + part;
  }

  return flux;
}
