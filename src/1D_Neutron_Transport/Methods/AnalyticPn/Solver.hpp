#include "Solver/Grid.hpp"
#include "Solver/EigenValues.hpp"
#include "Solver/ExpCoefficients.hpp"
#include "Solver/ParticularSolution.hpp"
#include "Solver/ScalarFlux.hpp"

template<typename Q>
auto solver(Q quad, double a, double b, double sigT, double c, int K, int n){
  std::clock_t start;
  start = std::clock();

  // ----- Set Import Parameters -----
  auto nodes = quad.getNodes();
  auto weigh = quad.getWeights();
  int N = nodes.size();
  double sigS = c*sigT;
  std::string sType;

  if (n == 0){
    sType = "n = 0";
  } else if (n == 1){
    sType = "n = 1";
  }

  // ----- Build Spatial Grid -----
  std::vector<double> grid = buildGrid(a, b, K);
  
  // ----- Build Eigenvalue system -----
  blaze::DynamicMatrix<double, blaze::columnMajor> V = buildV(sigT, sigS, N);
  blaze::DynamicMatrix<double, blaze::columnMajor> W = buildW(sigT, N);
  blaze::DynamicMatrix<double, blaze::columnMajor> Eigen;
  Eigen = (-1)*blaze::inv( W )*V;

  // ----- Solve for eigenvalues -----
  blaze::DynamicVector<blaze::complex<double>, blaze::columnVector> eigenValues(N);
  blaze::DynamicMatrix<blaze::complex<double>, blaze::columnMajor> eigenVec(N,N,0.0);  

  blaze::geev( Eigen, eigenValues, eigenVec );

  // ----- Make Particular Solution -----
  auto uniform   = getN0(sigT, sigS);
  auto quadratic = getN1(a, b, sigT, sigS); 
  
  // ----- Solve for Expansion Coefficients -----
  blaze::DynamicVector<double, blaze::columnVector> f = buildF(nodes, a, b, uniform, quadratic, sType);
  blaze::DynamicMatrix<double, blaze::columnMajor>  B = buildB(eigenValues, eigenVec, nodes, a, b, sigT);

  const std::unique_ptr<blaze::blas_int_t[]> ipiv( new blaze::blas_int_t[N] );
  blaze::gesv( B, f, ipiv.get() );

  
  // ----- Make Scalar Flux -----
  auto scalar = computeScalarFlux(eigenVec, eigenValues, f, nodes, weigh, grid, sigT, uniform, quadratic, sType);

  // ----- Output Finish Message ----- 
  double duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
  std::cout << "##############################################" << std::endl;
  std::cout << "######## ANALYTIC Pn SOLVER COMPLETE #########" << std::endl;
  std::cout << "##############################################" << std::endl;
  std::cout << "- run time: " << duration << " sec" << std::endl;
  std::cout << "##############################################" << std::endl << std::endl;
  
  return scalar;
}
