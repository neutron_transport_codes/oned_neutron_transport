template<typename T>
void printToFile(blaze::DynamicVector<double, blaze::columnVector> Flux, T&& grid, std::string name){
  int N = grid.size();

  std::ofstream myfile (name);

  if( myfile.is_open() ) {
    for( int i = 0; i < N; ++i ){
      myfile << std::setprecision(15) << grid[i] << " " << Flux[i] << "\n";
    }
  }
  myfile.close();
}

template<typename T>
void printToFile(T&& Flux, T&& grid, std::string name){
  int N = grid.size();

  std::ofstream myfile (name);

  if( myfile.is_open() ) {
    for( int i = 0; i < N; ++i ){
      myfile << std::setprecision(15) << grid[i] << " " << Flux[i] << "\n";
    }
  }
  myfile.close();
}
