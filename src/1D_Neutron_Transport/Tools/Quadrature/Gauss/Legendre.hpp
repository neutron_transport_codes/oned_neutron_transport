template<int N>
class Legendre {
private:
  enum {ORDER = N};
  std::vector<double> nodes;
  std::vector<double> weights;

  double polynomial(int n, double x){
    if( n <  0 ){
      return 0;
    } else if ( n == 0 ){
      return 1;
    } else if ( n == 1 ){
      return x;
    } else {
      double P; 
      double Pnn = 1;
      double Pn  = x;
      for(int k = 2; k <= n; ++k){
	P = ((2*static_cast<double>(k) - 1)*x*Pn - (static_cast<double>(k) - 1)*Pnn)/static_cast<double>(k);
	Pnn = Pn;
	Pn  = P;
      }
      return P;
    }
  }

  double dPolynomial(int n, double x){
    double Pn  = this->polynomial(n,   x);
    double Pnn = this->polynomial(n-1, x);
    return static_cast<double>(n)/(x*x - 1)*(x*Pn - Pnn);
  }
  
  void genNodes(){
    double eps = std::numeric_limits<double>::epsilon();
    for(int i = 0; i < this->ORDER; ++i){
      double xm = cos(PI*(i+1 - 0.25)/(this->ORDER + 0.5));
      double xn = 0;
      bool cond = true;
      int it = 0;
      do {
	xn = xm - this->polynomial(this->ORDER, xm)/this->dPolynomial(this->ORDER, xm);
	double err = abs((xn - xm)/xn);
	if( err <= eps || it >= 1e2 ){
	  cond = false;
	}
	xm = xn;
	++it;
      } while(cond);
      this->nodes.push_back(xn);
    }
  }

  void genWeights(){
    for(int i = 0; i < this->ORDER; ++i){
      double wi = 2/( (1 - this->nodes[i]*this->nodes[i])*(pow(this->dPolynomial(this->ORDER, this->nodes[i]), 2)) );
      this->weights.push_back(wi);
    }
  }

public:
  Legendre() {
    this->genNodes();
    this->genWeights();
  }
  
  auto Pn(){
    return [this](double x){ return std::legendre(this->ORDER, x); };
  }

  auto getNodes(){
    return this->nodes;
  }

  auto getWeights(){
    return this->weights;
  }

  int order(){
    return this->ORDER;
  }
  
  template<typename F>
  double integrate(F&& func, double a, double b){
    double sum = 0;
    double xi  = (b - a)/2;
    for(int i = 0; i < this->ORDER; ++i){
      sum += xi * this->weights[i] * ( func(xi*this->nodes[i] + (a + b)/2) );
    }
    return sum;
  }
};
