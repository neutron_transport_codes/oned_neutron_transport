template<int N>
class FuncLegendre {
private:
  enum {ORDER = N};

public:
  auto getNodes() {
    double eps = std::numeric_limits<double>::epsilon();
    auto rng   = ranges::view::ints(0, static_cast<int>(this->ORDER) );
    auto guess = rng | ranges::view::transform([this](int i){
						 return cos(PI*(static_cast<double>(i) + 1 - 0.25 )
							    /(static_cast<double>(this->ORDER) + 0.5 ) );} );
    
    auto poly = [](int n, double x){
		  if ( n < 0 ){
		    return static_cast<double>(0);
		  } else if ( n == 0 ){
		    return static_cast<double>(1);
		  } else if ( n == 1 ){
		    return static_cast<double>(x);
		  } else {
		    double P; 
		    double Pnn = 1;
		    double Pn  = x;
		    for(int k = 2; k <= n; ++k){
		      P = ((2*static_cast<double>(k) - 1)*x*Pn - (static_cast<double>(k) - 1)*Pnn)
			/static_cast<double>(k);
		      Pnn = Pn;
		      Pn  = P;
		    }
		    return P;
		  }
		};

    auto d_poly = [poly](int n, double x){
		    double Pn  = poly(n,   x);
		    double Pnn = poly(n-1, x);
		    return static_cast<double>(n)/(x*x - 1)*(x*Pn - Pnn);
		  };
    
    auto iter = [this, poly, d_poly, eps](double g){
    		  double xm = g;
    		  double xn = 0;
    		  bool cond = true;
    		  int it = 0;
    		  do {
    		    xn = xm - poly(this->ORDER, xm)/d_poly(this->ORDER, xm);
		    double err = abs((xn - xm)/xn);
		    if( err <= eps || it > 1e2 ){
		      cond = false;
		    }
		    xm = xn;
		    ++it;
		  } while(cond);
		  return xn;
		};

    auto nodes = guess | ranges::view::transform( iter );

    return nodes;
  }

  auto getWeights(){
    auto nodes = this->getNodes();

    auto poly = [](int n, double x){
		  if ( n < 0 ){
		    return static_cast<double>(0);
		  } else if ( n == 0 ){
		    return static_cast<double>(1);
		  } else if ( n == 1 ){
		    return static_cast<double>(x);
		  } else {
		    double P; 
		    double Pnn = 1;
		    double Pn  = x;
		    for(int k = 2; k <= n; ++k){
		      P = ((2*static_cast<double>(k) - 1)*x*Pn - (static_cast<double>(k) - 1)*Pnn)
			/static_cast<double>(k);
		      Pnn = Pn;
		      Pn  = P;
		    }
		    return P;
		  }
		};

    auto d_poly = [poly](int n, double x){
		    double Pn  = poly(n,   x);
		    double Pnn = poly(n-1, x);
		    return static_cast<double>(n)/(x*x - 1)*(x*Pn - Pnn);
		  };

    auto genWeights = [this, d_poly](double x){
			return 2/( (1 - x*x)*(pow(d_poly(this->ORDER, x), 2)) );
		      };

    auto weights = nodes | ranges::view::transform( genWeights );
    
    return weights;
  }
};
