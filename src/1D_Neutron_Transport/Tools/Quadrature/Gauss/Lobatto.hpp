template<int N>
class Lobatto {
private:
  enum {ORDER = N};
  std::vector<double> nodes;
  std::vector<double> weights;

  double polynomial(int n, double x){
    if( n <  0 ){
      return 0;
    } else if ( n == 0 ){
      return 1;
    } else if ( n == 1 ){
      return x;
    } else {
      double P; 
      double Pnn = 1;
      double Pn  = x;
      for(int k = 2; k <= n; ++k){
	P = ((2*static_cast<double>(k) - 1)*x*Pn - (static_cast<double>(k) - 1)*Pnn)/static_cast<double>(k);
	Pnn = Pn;
	Pn  = P;
      }
      return P;
    }
  }

  // Polynomial
  double poly(double x){
    return (static_cast<double>(this->ORDER) - 1)*(x*this->polynomial(this->ORDER-1, x)
						   - this->polynomial(this->ORDER-2, x) );
  }

  // dP(n-1)
  double dpoly(double x){
    return (static_cast<double>(this->ORDER) - 1)/(x*x - 1)*(x*this->polynomial(this->ORDER-1, x)
							     - this->polynomial(this->ORDER-2, x));
  }
  
  // dP(n-2)
  double dpolyn(double x){
    return (static_cast<double>(this->ORDER) - 2)/(x*x - 1)*(x*this->polynomial(this->ORDER-2, x)
							     - this->polynomial(this->ORDER-3, x) );
  }

   double dP(double x){
    return (static_cast<double>(this->ORDER) - 1)*(x*this->dpoly(x) + this->polynomial(this->ORDER-1, x)
						   - this->dpolyn(x) );
  }
  
  void genNodes(){
    double eps = std::numeric_limits<double>::epsilon(); 
    this->nodes.push_back(1.0);
    for(int i = 1; i < this->ORDER-1; ++i){
      double xm = cos(PI*(i+1 - 0.25)/(this->ORDER + 0.5));
      double xn = 0;
      bool cond = true;
      int it = 0;
      do {
      	xn = xm - this->poly(xm)/this->dP(xm);
      	double err = abs((xn - xm)/xn);
      	if( err <= eps || it >= 1e2 ){
      	  cond = false;
      	}
      	xm = xn;
      	it += 1;
      } while(cond);
      this->nodes.push_back(xm);
    }
    this->nodes.push_back(-1.0);
  }

  auto genWeights(){
    double w1 = 2/(static_cast<double>(this->ORDER)*(static_cast<double>(this->ORDER) - 1));
    this->weights.push_back(w1);
    double n = static_cast<double>(this->ORDER);
    for(int i = 1; i < this->ORDER-1; ++i){
      double Pn = (2*(n-1) - 1)/(n - 1)*this->nodes[i]*this->polynomial(this->ORDER-2, this->nodes[i])
	- ((n-1) - 1)/(n - 1)*this->polynomial(this->ORDER-3, this->nodes[i]);
      double w = 2/((n*(n-1))*(Pn*Pn));
      this->weights.push_back(w);
    }
    this->weights.push_back(w1);
  }
  
public:
  Lobatto() {
    this->genNodes();
    this->genWeights();
  }

  auto getNodes(){
    return this->nodes;
  }

  auto getWeights(){
    return this->weights;
  }
  
  template<typename F>
  double integrate(F&& func, double a, double b){
    double sum = 0;
    double xi  = (b - a)/2;
    for(int i = 0; i < this->ORDER; ++i){
      sum += xi * this->weights[i] * ( func(xi*this->nodes[i] + (a + b)/2) );
    }
    return sum;
  }
};
