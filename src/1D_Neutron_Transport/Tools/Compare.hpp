double compare(std::vector<double> flux1, std::vector<double> flux2, int K){
  double sum = 0;
  for( int i = 0; i < K; ++i ){
    sum += pow( flux1[i] - flux2[i], 2.0 );
  }
  return pow( (1.0/K)*sum, 0.5 );
}
