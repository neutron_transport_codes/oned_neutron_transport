#ifndef OD_NTR_HPP
#define OD_NTR_HPP

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <math.h> 
#include <ctime>
#include <string>

#include <blaze/Math.h>
#include <range/v3/all.hpp>
#include <boost/math/quadrature/gauss_kronrod.hpp>
#include <boost/hana/functional/curry.hpp>

namespace OD_NTR {

#include "1D_Neutron_Transport/Tools.hpp"
#include "1D_Neutron_Transport/Methods.hpp"
  
}

#endif
