#include "1D_Neutron_Transport.hpp"

int main(){
#include "input.hpp"

  // -------------------- BUILD GRID ----------------------
  std::vector<double> grid(K+1, 0.0);
  std::vector<double> center(K, 0.0);
  std::vector<double> manufactured(K, 0.0);
  
  for(int i = 0; i < K+1; ++i){
    grid[i] = A + (static_cast<double>(i))/(K)*(B - A);
  }

  for(int i = 0; i < K; ++i){
    double xL = grid[i+0];
    double xR = grid[i+1];
    
    center[i] = 0.5*(xL + xR);
    manufactured[i] = 1.0/(xR - xL)*((B/2.0)*(xR*xR - xL*xL) - (1.0/3.0)*(xR*xR*xR - xL*xL*xL));
  }

  // -------------------- ANALYTIC Sn ---------------------
  std::vector< std::vector<double> > RESULT;
  std::vector<double> GRID_INTEGRAL;
  std::vector<double> FLUX_INTEGRAL;
  if (INTEGRAL_EQ) {
    RESULT = OD_NTR::IntegralEq::solver(quad, A, B, SIG_T, C, SOURCE);
    GRID_INTEGRAL = RESULT[0];
    FLUX_INTEGRAL = RESULT[1];
    OD_NTR::Tools::printToFile(FLUX_INTEGRAL, GRID_INTEGRAL, "integralEq.txt");
  }
  
  // -------------------- ANALYTIC Sn ---------------------
  std::vector<double> FLUX_ANA_Sn;
  if (ANALYTIC_Sn) {
    FLUX_ANA_Sn = OD_NTR::AnalyticSn::solver(quad, A, B, SIG_T, C, K, nS); 
    OD_NTR::Tools::printToFile(FLUX_ANA_Sn, center, "analyticSn.txt");
  }
  
  // -------------------- ANALYTIC Pn ---------------------
  std::vector<double> FLUX_ANA_Pn;
  if (ANALYTIC_Pn) {
    FLUX_ANA_Pn = OD_NTR::AnalyticPn::solver(quad, A, B, SIG_T, C, K, nP);
    OD_NTR::Tools::printToFile(FLUX_ANA_Pn, center, "analyticPn.txt");
  }
  
  // -------------------- NUMERICAL Sn --------------------
  std::vector<double> FLUX_NUM_Sn;
  if (NUMERICAL_Sn) {
    OD_NTR::NumericalSn<decltype(quad), decltype(LEFT_BOUNDARY), decltype(RIGH_BOUNDARY), decltype(SOURCE)>
      NUM_Sn(quad, A, B, SIG_T, C, K, LEFT_BOUNDARY, RIGH_BOUNDARY, SOURCE);
    NUM_Sn.solve(TOLERANCE, DSA);
    FLUX_NUM_Sn = NUM_Sn.getScalar();  
    OD_NTR::Tools::printToFile(FLUX_NUM_Sn, center, "numericalSn.txt");
  }
 
  // -------------------- NUMERICAL P1 --------------------
  std::vector<double> FLUX_NUM_P1;
  std::vector<double> CURR_NUM_P1;
  if (NUMERICAL_P1) {
    OD_NTR::NumericalP1<decltype(quad), decltype(SOURCE_0), decltype(SOURCE_1)> NUM_P1(quad, A, B, SIG_T,
										       C, K, SOURCE_0, SOURCE_1);
    NUM_P1.solve();
    FLUX_NUM_P1 = NUM_P1.getFlux();
    CURR_NUM_P1 = NUM_P1.getCurrent();
    OD_NTR::Tools::printToFile(FLUX_NUM_P1, center, "numericalP1.txt");
  }

  // ---------------- Compare Solutions --------------------
  // double error_P = OD_NTR::Tools::compare(manufactured, FLUX_NUM_P1, K);
  // double error_J = OD_NTR::Tools::compare(manufactured, CURR_NUM_P1, K);

  // std::cout.precision(15);
  // std::cout << std::scientific << error_P << " " << error_J << std::endl;
  return 0;
}
