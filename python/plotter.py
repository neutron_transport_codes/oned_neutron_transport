import numpy as np
import matplotlib.pyplot as plt

from reader import read

def main():

    result = read()
    plt.figure(1)
    
    make_plot = 0
    if result[0] == True:
        integralEquation = np.loadtxt('../build/code/integralEq.txt', usecols=range(1,1))
        plt.plot(integralEquation[:,0], integralEquation[:,1], label='Integral Equation')
        make_plot = 1
    if result[1] == True:
        analyticSn = np.loadtxt('../build/code/analyticSn.txt', usecols=range(1,1))
        plt.plot(analyticSn[:,0], analyticSn[:,1], label='Analytical Sn')
        make_plot = 1
    if result[2] == True:
        analyticPn = np.loadtxt('../build/code/analyticPn.txt', usecols=range(1,1))
        plt.plot(analyticPn[:,0], analyticPn[:,1], label='Analytical Pn')
        make_plot = 1
    if result[3] == True:
        numericalSn = np.loadtxt('../build/code/numericalSn.txt', usecols=range(1,1))
        plt.plot(numericalSn[:,0], numericalSn[:,1], label='Numerical Sn')
        make_plot = 1
    if result[4] == True:
        numericalP1 = np.loadtxt('../build/code/numericalP1.txt', usecols=range(1,1))
        plt.plot(numericalP1[:,0], numericalP1[:,1], label='Numerical P1')
        make_plot = 1
        
    if make_plot == 1:
        plt.xlabel('x (cm)')
        plt.ylabel('$\Phi(x)$')
        plt.title('$\Phi(x)$')
        plt.legend()
        plt.show()
    

if __name__ == '__main__': main()
