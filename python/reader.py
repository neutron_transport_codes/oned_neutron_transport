def read():
    filepath = '../code/input.hpp'

    loc1 = []
    loc2 = []
    loc3 = []
    loc4 = []
    loc5 = []
    
    with open(filepath, 'rt') as f:
        lines = []
        ls  = 0
        for line in f:
            lines.append(line.rstrip('\n'))
        for vect in lines:
            loc1.append(vect.find("INTEGRAL_EQ"))
            loc2.append(vect.find("ANALYTIC_Sn"))
            loc3.append(vect.find("ANALYTIC_Pn"))
            loc4.append(vect.find("NUMERICAL_Sn"))
            loc5.append(vect.find("NUMERICAL_P1"))

    N = len(loc1)

    ls1 = 0
    ls2 = 0
    ls3 = 0
    ls4 = 0
    ls5 = 0

    for i in range(0, N):
        if loc1[i] != -1:
            ls1 = i
        if loc2[i] != -1:
            ls2 = i
        if loc3[i] != -1:
            ls3 = i
        if loc4[i] != -1:
            ls4 = i
        if loc5[i] != -1:
            ls5 = i

    loc_list = []
    loc_list.append(lines[ls1].find("true"))
    loc_list.append(lines[ls2].find("true"))
    loc_list.append(lines[ls3].find("true"))
    loc_list.append(lines[ls4].find("true"))
    loc_list.append(lines[ls5].find("true"))

    result = []
    for i in range(0,5):
        if loc_list[i] == -1:
            result.append(False)
        else:
            result.append(True)

    return result

    
    
    
